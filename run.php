<?php
require __DIR__ . '/vendor/autoload.php';

$options['l'] = TRUE;
$options['no-locale'] = TRUE;

$opts = getopt('l', ['no-locale']);

$options = array_merge($options, $opts);

function out($locale, $str) {
    global $options;
    if (!$options['l']
        || !$options['no-locale']) {
        return print("{$str}\n");
    }
    return print("[{$locale}] {$str}\n");
}

$locales = [
    'en_US', //Estados Unidos
    'en_GB', //Inglaterra
    'es_AR', //Argentina
    'es_ES', //Espanha
    'es_PE', //Peru
    'fr_FR', //França
    'ja_JP', //Japão
    'pt_BR', //Brasil
    'pt_PT', //Portugal
    'de_DE', //Alemanhã
    'it_IT', //Itália
];

foreach ($locales as $locale) {
    $Faker = Faker\Factory::create($locale);
    for ($i = 0; $i <= rand(1,4); $i++) {
        out($locale, $Faker->phoneNumber);
        out($locale, $Faker->e164PhoneNumber);
    }
}

